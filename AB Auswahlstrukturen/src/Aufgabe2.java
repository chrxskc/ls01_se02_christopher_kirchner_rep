import java.util.Scanner;
public class Aufgabe2 {

	public static void main(String[] args) {
		//Nettowerteingabe 
		double nettowert = 0;
		nettowert = Nettowerteingabe (nettowert);
		
		//Steuersatzerfassung 
		double steuersatz = 0;
		steuersatz = Steuersatzerfassen (nettowert, steuersatz);
		
		//Bruttoberechnung 
		double bruttowert = 0;
		bruttowert = bruttoberechnung (nettowert, steuersatz);
		
		//Ausgabe 
		String euro = "�";
		System.out.println("Ihr Gesamtpreis betr�gt nun: " + bruttowert + euro);
	}
	
	public static double Nettowerteingabe (double nettowert) {
		Scanner tastatur = new Scanner (System.in);
		System.out.println("Bitte Geben Sie den Nettowert ein: ");
		nettowert = tastatur.nextDouble();
		return nettowert;
	}

	public static double Steuersatzerfassen (double nettowert, double steuersatz) {
		Scanner steuersatzeingabe = new Scanner (System.in);
		System.out.println("Bitte w�hlen Sie den Steuersatz f�r den Betrag von: " + nettowert);
		System.out.println("Dr�cken Sie 'j' f�r den erm��igten und 'n' f�r den vollen Steuersatz: " + steuersatzeingabe);
		String steuer = steuersatzeingabe.next();
		steuersatz = 0;
		if (steuer.equals("j")) {
			steuersatz = 0.07;
			return steuersatz;
		}
		else {
			steuersatz = 0.19;
			return steuersatz;
		}
	}

	public static double bruttoberechnung (double nettowert,double steuersatz) {
		double bruttowert = (nettowert * steuersatz) + nettowert;
		return bruttowert;
	}

}

