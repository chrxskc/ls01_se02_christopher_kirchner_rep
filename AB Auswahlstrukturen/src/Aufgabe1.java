
public class Aufgabe1 {

	public static void main(String[] args) {
		
		int zahl1 = 100;
		int zahl2 = 200;
		int zahl3 = 300;
		
		//Nennen Sie Wenn-Dann-Aktivit�ten aus ihrem Alltag
		
		//Wenn beide Zahlen gleich sind, soll eine Meldung ausgegeben werden.
		if (zahl1 == zahl2) {
			System.out.println("Die Zahlen sind gleich gro�.");
		}
		else {
			System.out.println("Die Zahlen sind nicht gleich gro�.");
		}
	
		
		//Wenn die 2.Zahl gr��er ist als die 1.Zahl ist, soll eine Meldung ausgegeben werden.
		if (zahl1 > zahl2) {
			System.out.println("Zahl 1 ist gr��er, als Zahl 2.");
		}
		else {
			System.out.println("Zahl 2 ist gr��er, als Zahl 1.");
		}
		
		
		//Wenn die 1.Zahl gr��er ist als die 2.Zahl ist, soll eine Meldung ausgegeben werden.
		if (zahl1 < zahl2) {
			System.out.println("Zahl 2 ist gr��er, als Zahl 1.");
		}
		else {
			System.out.println("Zahl 1 ist gr��er, als Zahl 2.");
		}

		
		//Wenn die 1.Zahl gr��er als die 2.Zahl und die 3. Zahl ist, soll eine Meldung ausgegeben werden.
		if (zahl1 > zahl2 && zahl1 > zahl3) {
			System.out.println("Die Zahl 1 ist am gr��ten.");
		}
		else {
			System.out.println("Zahl 1 ist nicht die gr��te der 3 Zahlen.");
		}
		
		
		//Wenn die 3.Zahl gr��er als die 2.Zahl oder die 1. Zahl ist, soll eine Meldung ausgegeben werden.
		if (zahl3 > zahl1 || zahl3 > zahl2) {
			System.out.println("Die Zahl 3 ist gr�0er als die Zahl 1 oder Zahl 2");
		}
		else {
			System.out.println("Die Zahl 3 ist nicht gr��er, als Zahl 1 oder Zahl 2.");
		}
		
		
		//Geben Sie die gr��te der 3 Zahlen aus.
		if (zahl1 > zahl2 && zahl1 > zahl3) {
			System.out.println("Die Zahl1 ist am gr��ten.");
		}
		if (zahl2 > zahl1 && zahl2 > zahl3) {
			System.out.println("Die Zahl 2 ist am gr��ten.");
		}
		else {
			System.out.println("Die Zahl 3 ist am gr��ten.");
		}
	}	
		
}
	

