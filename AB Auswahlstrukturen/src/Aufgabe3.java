import java.util.Scanner;
public class Aufgabe3 {

	public static void main(String[] args) {
		//Bestellung aufgeben
		double anzahlmäuse = 0;
		anzahlmäuse = EingabeAnzahlMaus(anzahlmäuse);
		
		//Angabe Einzelpreis
		double einzelpreis = 0;
		einzelpreis = Preisangabe (einzelpreis);

		//Berechnung Lieferpauschale 
		double lieferpauschale = 0;
		lieferpauschale = Lieferpauschale (anzahlmäuse, lieferpauschale);
		
		//Berechnung Gesamtpreis
		double gesamtpreis = 0;
		gesamtpreis = Gesamtpreisberechnung (anzahlmäuse, einzelpreis, lieferpauschale, gesamtpreis);
		
		//Ausgabe 
		String euro = "€";
		System.out.println("Der Gesamtpreis Ihrer Bestellung beträgt " + gesamtpreis + euro);
	}
	
	public static double EingabeAnzahlMaus(double anzahlmäuse) {
		Scanner mausanzahl = new Scanner (System.in);
		System.out.println("Wie viele Mäuse möchten Sie bestellen? \n");
		anzahlmäuse = mausanzahl.nextDouble();
		return anzahlmäuse;
	}
	
	public static double Preisangabe (double einzelpreis) {
		Scanner preisangabe = new Scanner (System.in);
		System.out.println("Wie teuer ist eine einzelne Maus? \n");
		einzelpreis = preisangabe.nextDouble();
		return einzelpreis;
	}
	
	public static double Lieferpauschale (double anzahlmäuse, double lieferpauschale) {
		if (anzahlmäuse >= 10) {
			lieferpauschale = 0;
			return lieferpauschale;
		}
		else {
			lieferpauschale = 10;
			return lieferpauschale;
		}
	}
	
	public static double Gesamtpreisberechnung (double anzahlmäuse, double einzelpreis, double lieferpauschale, double gesamtpreis) {
		gesamtpreis = (anzahlmäuse * einzelpreis) + lieferpauschale;
		return gesamtpreis;
	}
	
}
