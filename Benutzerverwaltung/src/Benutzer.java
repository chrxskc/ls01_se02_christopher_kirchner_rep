import java.text.SimpleDateFormat;
import java.util.Date;

public class Benutzer {

	private int benutzernummer;
	private String vorname;
	private String nachname;
	private int geburtsjahr;

	public Benutzer(int benutzernummer, String vorname, String nachname, int geburtsjahr) {
		this.benutzernummer = benutzernummer;
		this.vorname = vorname;
		this.nachname = nachname;
		this.geburtsjahr = geburtsjahr;
	}

	public void getBenutzernummer(int benutzernummer) {
		this.benutzernummer = benutzernummer;
	}

	public int setBenutzernummer() {
		return benutzernummer;
	}

	public void getVorname(String vorname) {
		this.vorname = vorname;
	}
	
	public String setVorname() {
		return vorname;
	}
	
	public void getNachname(String nachname) {
		this.nachname = nachname;
	}
	
	public String setNachname() {
		return nachname;
	}
	
	public void getGeburtsjahr(int geburtsjahr) {
		this.geburtsjahr = geburtsjahr;
	}
	
	public int setGeburtsjahr() {
		return geburtsjahr;
	}
	
	public int getAlter() {
		SimpleDateFormat formatter = new SimpleDateFormat("YYYY");
		Date date = new Date();
		int aYear = Integer.parseInt(formatter.format(date));
		int alter = aYear - geburtsjahr;
		return alter;
	}
	
	@Override
	public String toString() {
		return "ID: " + this.benutzernummer + " Vorname: " + this.vorname + " Nachname: " + this.nachname + " Geburtsjahr: " + this.geburtsjahr;
	}
	
}
