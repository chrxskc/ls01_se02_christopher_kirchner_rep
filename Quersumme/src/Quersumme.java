import java.util.Scanner;

public class Quersumme {

		public static void main(String[] args) {
			Scanner tastatur = new Scanner (System.in);
			System.out.print("Bitte geben Sie eine Zahl ein: ");
			int zahl = tastatur.nextInt();
			System.out.println("Die Quersumme betr�gt: " + quersumme(zahl));
		}

		public static int quersumme(int zahl) {
			if (zahl <= 9) return zahl;
			return zahl%10 + quersumme(zahl/10);
		}

	}

