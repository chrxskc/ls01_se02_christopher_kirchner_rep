import java.util.Scanner;

public class Aufgabe4 {
	// Aufgabe 4: Temperaturumrechnung

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner (System.in);
		
		System.out.print("Bitte den Startwert in Celsius eingeben: ");
		
		double startwert = tastatur.nextInt();
		
		System.out.print("Bitte den Endwert in Celsius eingeben: ");
		
		double endwert = tastatur.nextInt();
		
		System.out.print("Bitte die Schrittweite in Grad Celsius eingeben: ");
		
		double schrittweite = tastatur.nextInt();
		
		double cwert = startwert;
		double fwert = 0;
		
		while (cwert <= endwert) 
		{
			fwert = (cwert*1.8)+32;
			System.out.printf(cwert +" "+ fwert);
			cwert = cwert + schrittweite;
		}
	}

}
