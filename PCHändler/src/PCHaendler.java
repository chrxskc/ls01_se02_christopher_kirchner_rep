import java.util.Scanner;

public class PCHaendler {

	public static void main (String[] args) {
	
		Scanner myScanner = new Scanner(System.in);
	
	// Benutzereingaben lesen
	
		//System.out.println("was m�chten Sie bestellen?");
		//String artikel = myScanner.next();
		String bestellung = leseArtikel("Was m�chten Sie bestellen?", myScanner);
		
		//System.out.println("Geben Sie die Anzahl ein:");
		//int anzahl = myScanner.nextInt();
		int anzahl = leseAnzahl("Geben Sie die Anzahl ein: ", myScanner);
			
		//System.out.println("Geben Sie den Nettopreis ein:");
		//double preis = myScanner.nextDouble();
		double preis = lesePreis("Geben Sie den Nettopreis ein: ", myScanner);
			
		//System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		//double mwst = myScanner.nextDouble();
		double mwst = leseMwst("Geben Sie den Mehrwertsteuersatz ein: ", myScanner);
	
	
	// Verarbeiten
		double nettogesamtpreis = anzahl * preis;
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
	
			
	// Ausgeben
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", bestellung, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", bestellung, anzahl, bruttogesamtpreis, mwst, "%");	
					
	}	
	
	
	
	
	//Methoden Benutzereingabe
	
		public static String leseArtikel(String text, Scanner myScanner) {
			System.out.println(text);
			String artikel = myScanner.next();
			return artikel;
	}
	
		public static int leseAnzahl(String text, Scanner myScanner) {
			System.out.println(text);
			int anzahl = myScanner.nextInt();
			return anzahl;
	}
	
		public static double lesePreis(String text, Scanner myScanner) {
			System.out.println(text);
			double preis = myScanner.nextDouble();
			return preis;	
	}
		
		public static double leseMwst(String text, Scanner myScanner) {
			System.out.println(text);
			double mwst = myScanner.nextDouble();
			return mwst;
	}


	
	
	//Methoden Verarbeitung 
	
		public static double berechneNettogesamtpreis(int anzahl, double preis) {
			double nettogesamtpreis = anzahl * preis;
			System.out.println(nettogesamtpreis);
			return nettogesamtpreis;
	}
	
		public static double berechneBruttogesamtpreis(double nettogesamtpreis, double mwst) {
			double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
			System.out.println(bruttogesamtpreis);
			return bruttogesamtpreis;
	}	
		
		
	//Methoden Ausgabe
	
		public static void rechnungausgaben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
			System.out.println("\tRechnung");
			System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
			System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}	
}     
		
