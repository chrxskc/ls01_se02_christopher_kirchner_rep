﻿import java.util.Scanner;

class Fahrkartenautomat
{
	public static void main(String[] args)
    {
		//Fahrscheinerfassung 
		double[] arraypreise = {2.9,3.3,3.6,1.9,8.6,9.0,9.6,23.5,24.3,24.9};
		String[] arrayfahrscheinbez = {"Einzelfahrschein Berlin ABC","Einzelfahrschein Berlin BC","Einzelfahrschein ABC","Kurzstrecke","Tageskarte Berlin AB","Tageskarte Berlin BC","Tageskarte Berlin ABC","Kleingruppen-Tageskarte-Berlin AB","Kleingruppen-Tageskarte Berlin BC","Kleingruppen-Tageskarte Berlin ABC"};
		
		double fahrscheinpreis = 0;
		fahrscheinpreis = FahrscheinartErfassen (fahrscheinpreis, arraypreise, arrayfahrscheinbez);
		
		//Erfassung Anzahl Tickets, Berechnung des Gesamtpreises
		double gesamtpreis = 0; 
	    gesamtpreis = fahrkartenbestellungErfassen(fahrscheinpreis, gesamtpreis);
	    
	    
	    //Bezahlung der Fahrkarten, Geldeinwurf
	   double eingezahlterGesamtbetrag = 0;
	   double eingeworfeneMünze = 0;
	   double rückgabebetrag = 0;
	   rückgabebetrag = FahrkartenBezahlen (gesamtpreis, eingezahlterGesamtbetrag, eingeworfeneMünze, rückgabebetrag);
	    
	   // Fahrscheinausgabe
	   Fahrscheinausgabe ();
	   
	   // Rückgeldberechnung und -Ausgabe
	   Rückgeldberechnung (rückgabebetrag);
    }

		public static double FahrscheinartErfassen (double fahrscheinpreis, double[] arraypreise, String[] arrayfahrscheinbez) {
			
		Scanner Fahrscheinauswahl = new Scanner (System.in);
		System.out.println("Welche Art von Fahrschein möchten Sie bestellen? Wählen Sie zwischen "
				+ "\n 1) " + arrayfahrscheinbez[0] + ","
				+ "\n 2) " + arrayfahrscheinbez[1] + "," 
				+ "\n 3) " + arrayfahrscheinbez[2] + ","
				+ "\n 4) " + arrayfahrscheinbez[3] + ","
				+ "\n 5) " + arrayfahrscheinbez[4] + ","
				+ "\n 6) " + arrayfahrscheinbez[5] + ","
				+ "\n 7) " + arrayfahrscheinbez[6] + ","
				+ "\n 8) " + arrayfahrscheinbez[7] + ","
				+ "\n 9) " + arrayfahrscheinbez[8] + ","
				+ "\n 10) " + arrayfahrscheinbez[9] + ".");
		
		String Fahrscheinart = Fahrscheinauswahl.next();
		if (Fahrscheinart.equals("1")) {
			System.out.println("Sie haben eine " + arrayfahrscheinbez[0] + " gewählt, der Preis beträgt 2.90€.");
			fahrscheinpreis = arraypreise [0];
			return fahrscheinpreis;
		}
		if (Fahrscheinart.equals("2")) {
			System.out.println("Sie haben eine " + arrayfahrscheinbez[1] + " gewählt, der Preis beträgt 3.30€.");
			fahrscheinpreis = arraypreise[1];
			return fahrscheinpreis;
		}
		if (Fahrscheinart.equals("3")) {
			System.out.println("Sie haben einen " + arrayfahrscheinbez[2] + " gewählt, der Preis beträgt 3.60€.");
			fahrscheinpreis = arraypreise[2];
			return fahrscheinpreis;
		}
		if (Fahrscheinart.equals("4")) {
			System.out.println("Sie haben einen " + arrayfahrscheinbez[3] + " gewählt, der Preis beträgt 1.90€.");
			fahrscheinpreis = arraypreise[3];
			return fahrscheinpreis;
		}
		if (Fahrscheinart.equals("5")) {
			System.out.println("Sie haben einen " + arrayfahrscheinbez[4] + " gewählt, der Preis beträgt 8.60€.");
			fahrscheinpreis = arraypreise[4];
			return fahrscheinpreis;
		}
		if (Fahrscheinart.equals("6")) {
			System.out.println("Sie haben einen " + arrayfahrscheinbez[5] + " gewählt, der Preis beträgt 9.00€.");
			fahrscheinpreis = arraypreise[5];
			return fahrscheinpreis;
		}
		if (Fahrscheinart.equals("7")) {
			System.out.println("Sie haben einen " + arrayfahrscheinbez[6] + " gewählt, der Preis beträgt 9.60€.");
			fahrscheinpreis = arraypreise[6];
			return fahrscheinpreis;
		}
		if (Fahrscheinart.equals("8")) {
			System.out.println("Sie haben einen " + arrayfahrscheinbez[7] + " gewählt, der Preis beträgt 23.50€.");
			fahrscheinpreis = arraypreise[7];
			return fahrscheinpreis;
		}
		if (Fahrscheinart.equals("9")) {
			System.out.println("Sie haben einen " + arrayfahrscheinbez[8] + " gewählt, der Preis beträgt 24.30€.");
			fahrscheinpreis = arraypreise[8];
			return fahrscheinpreis;
		}	
		if (Fahrscheinart.equals("10")) {
			System.out.println("Sie haben einen " + arrayfahrscheinbez[9] + " gewählt, der Preis beträgt 24.90€.");
			fahrscheinpreis = arraypreise[9];
			return fahrscheinpreis;
		}
		return fahrscheinpreis;
    }
		
		public static double fahrkartenbestellungErfassen(double fahrscheinpreis, double gesamtpreis) {
		Scanner fahrkartenanzahl = new Scanner (System.in);
		System.out.println("Wie viele Fahrkarten möchten Sie bestellen?");
		double anzahlfahrkarten = fahrkartenanzahl.nextDouble();
	    gesamtpreis = fahrscheinpreis * anzahlfahrkarten;
		return gesamtpreis;
		}

		public static double FahrkartenBezahlen (double gesamtpreis, double eingezahlterGesamtbetrag, double eingeworfeneMünze, double rückgabebetrag) {
			Scanner tastatur = new Scanner (System.in);
		       while(eingezahlterGesamtbetrag < gesamtpreis) 
		       {
		    	   System.out.printf("Noch zu zahlen: %4.2f Euro \n", (gesamtpreis - eingezahlterGesamtbetrag));
		    	   System.out.print("Eingabe (mind. 5Ct, höchstens 10 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
		       }
		       rückgabebetrag = eingezahlterGesamtbetrag - gesamtpreis;
	           return rückgabebetrag;
		}
			   
	    public static void Fahrscheinausgabe () {

	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			}
	          catch (InterruptedException e) {
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	       }
	       
	    public static void Rückgeldberechnung (double rückgabebetrag) {
	       
	       
	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der Rückgabebetrag in Höhe von %4.2f Euro ", rückgabebetrag);
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");
	    	   
	    	   while(rückgabebetrag >= 10.0) // 10 EURO-Schein
	    	   {
	    		   System.out.println("10 Euro");
	    		   rückgabebetrag -= 10.0;
	    	   }
	    	   while(rückgabebetrag >= 5.0) // 5 EURO-Schein
	    	   {
	    		   System.out.println("5 Euro");
	    		   rückgabebetrag -= 5.0;
	    	   }
	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.");
	    }
}
		
		
		
		
		
		
		

	
