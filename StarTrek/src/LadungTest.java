
public class LadungTest {

	public static void main(String[] args) {

		// Ladung l1

		Ladung l1 = new Ladung();
		l1.setBezeichnung("Ferengi Schneckensaft");
		l1.setMenge(200);

		System.out.println("Ladung 1: ");
		System.out.println(l1.getBezeichnung());
		System.out.println(l1.getMenge());

		// Ladung l2

		Ladung l2 = new Ladung("Borg-Schrott", 5);
		System.out.println("Ladung 2: ");
		System.out.println(l2.getBezeichnung());
		System.out.println(l2.getMenge());

		// Ladung l3

		Ladung l3 = new Ladung("Rote Materie", 2);
		System.out.println("Ladung 3: ");
		System.out.println(l3.getBezeichnung());
		System.out.println(l3.getMenge());

		// Ladung l4

		Ladung l4 = new Ladung ("Forschungssonde", 35);
		System.out.println("Ladung 4: ");
		System.out.println(l4.getBezeichnung());
		System.out.println(l4.getMenge());
		
		// Ladung l5
		
		Ladung l5 = new Ladung ("Bat'leth Klingonen Schwert", 200);
		System.out.println("Ladung 5: ");
		System.out.println(l5.getBezeichnung());
		System.out.println(l5.getMenge());
		
		// Ladung l6
		
		Ladung l6 = new Ladung ("Plasma-Waffe", 50);
		System.out.println("Ladung 6: ");
		System.out.println(l6.getBezeichnung());
		System.out.println(l6.getMenge());
		
		// Ladung l7
		Ladung l7 = new Ladung ("Photonentorpedo", 3);
		System.out.println("Ladung 7: ");
		System.out.println(l7.getBezeichnung());
		System.out.println(l7.getMenge());
	}

}
