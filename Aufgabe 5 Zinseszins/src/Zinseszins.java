import java.util.Scanner;
public class Zinseszins {

	public static void main(String[] args) {
		double anfKap;
		double zins;
		double endKap;
		double q; 

		Scanner tastatur = new Scanner (System.in);
		
	     //Eingabe
			System.out.print("Laufzeit (in Jahren) des Sparvertrags: ");
			int lauf = tastatur.nextInt();
	    
			System.out.print("Wie viel Kapital m�chten Sie investieren? ");
			anfKap = tastatur.nextDouble();

			System.out.print("Zinssatz in Prozent : ");
			zins = tastatur.nextDouble();

	     
	    //Verarbeitung
			q = 1 + zins/100 ;
			endKap = anfKap*Math.pow(q, lauf);  // Berechnung
			endKap  = Math.rint(endKap*100);   // Rundung
			endKap = endKap/100 ;
	      
		//Ausgabe
			System.out.println("Eingezahltes Kapital : " + anfKap + " Euro");
			System.out.println("Ausgezahltes Kapital : " + endKap + " Euro");

	  }  

	}  
